Je propose d'aborder les points suivants en démo et rétro

# Sprint 0

Démo : au moins le radiateur d'informations physique et numérique et la présentation du projet. Il doit y avoir les 19 US déjà présentes dans gitlab de reporté sur leur management visuel.

Pas de rétro.

# Sprint 1 à 8

## Démo

* État du management visuel / radiateur d'information 
* le backlog, voir avec les étudiants ce qui a bougé durant le sprint, ce qui devait être fait, ce qui a été fait.
* le burndown chart
* le ou les PDCA en cours
* une démo de ce qui a été fait depuis la dernière démo

## Rétrospective + enseignement

voir le docmuent [support intervenant de mars](https://gitlab.com/tclavier/projet-agile-iut/-/blob/master/projet-de-mars/support-intervenants.md#r%C3%A9tro)


