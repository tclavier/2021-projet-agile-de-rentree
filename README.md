# Projet de rentrée 2021

## Quelques liens

* [Les slides d'introduction](https://tclavier.gitlab.io/2021-projet-agile-de-rentree/)
* [Le suivi de PDCA](./slides/includes/suivi-pdca.svg)

## Les groupes

[Voir les slides d'introduction.](https://tclavier.gitlab.io/2021-projet-agile-de-rentree/index.html#/groupes)

##
