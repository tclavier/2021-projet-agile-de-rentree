# Projet de rentrée 2021
2,5 jours pour replonger dans le code

# Liens

* [https://gitlab.univ-lille.fr/2021-projet-agile-de-rentree](https://gitlab.univ-lille.fr/2021-projet-agile-de-rentree)
* [https://tclavier.gitlab.io/2021-projet-agile-de-rentree](https://tclavier.gitlab.io/2021-projet-agile-de-rentree)

# Planning

|               | 01/09    | 02/09    | 03/09    |
|---------------|----------|----------|----------|
|  8h00 - 10h00 | Intro    | Sprint 2 | Sprint 6 |
| 10h00 - 12h00 | TP       | Sprint 3 | Démo / Rétro |
| 13h30 - 15h30 | Sprint 0 | Sprint 4 |          |
| 15h30 - 17h30 | Sprint 1 | Sprint 5 |          |

# Ce qu'il y a à faire

* Démonstrer **toutes** les issues déjà présente.
* Coder un jeu en java.
* Appliquer du scrum allégé.

# **Être agile** et pas faire de l’Agile.

C’est avant tout un état d'esprit partagé par l’ensemble des participants à un projet.

# 

:::: {.columns}
::: {.column width="80%"}
<blockquote>
    Les firmes qui survivent dans le long terme ne sont pas celles qui sont les plus fortes ou les plus intelligentes, mais celles qui s'adaptent le mieux aux changements d'environnement
    <footer>Hiroshi Okuda</footer>
</blockquote>
:::
::: {.column width="20%"}
![](./includes/Hiroshi_Okuda.jpg)
:::
::::


# Le manifest agile

* **Individuals and interactions** over processes and tools
* **Working software** over comprehensive documentation
* **Customer collaboration** over contract negotiation
* **Responding to change** over following a plan

# Valeurs et principes

Des cycles courts, 
un produit en production à chaque fin de cycle 
et des producteurs de valeurs qui s’améliorent continuellement.

# Scrum : déroulé 
![](./includes/scrum1.png){height=500px}

# Scrum : rituels
![](./includes/scrum2.png){height=500px}

# Scrum : rôles
![](./includes/scrum3.png){height=500px}

# Scrum : projet
![](./includes/scrum4.png){height=500px}


# Montrer
46 - 32 - 13 - 10 - 31 - 6 - 14 - 29 - 28 - 41 - 25 - 27 - 39 - 44 - 2 - 20 - 9 - 45 - 3 - 35 - 42 - 36 - 37 - 26 - 16 - 34 - 40 - 1 - 49 - 17 - 47 - 30 - 33 - 4 - 19 - 43 - 8 - 12 - 21 - 50 - 7 - 11 - 15 - 18 - 38 - 24 - 22 - 5 - 48

# Les problèmes

| | | | | | | | | | |
|-|-|-|-|-|-|-|-|-|-|
| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 
| 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 
| 21 | 22 |    | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 
| 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 
| 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 |

# Le radiateur

* Donner les moyens aux acteurs d’être autonomes
* Faire rayonner l’information
  * Vision du projet, Proposition de valeur unique
  * Date de livraison, avancement, etc.
  * Actions d’améliorations
  * Backlog
  * Problèmes
  * etc.

# Le radiateur
![](./includes/radiateur.png){height=500px}

# Version gitlab
![](./includes/kanban-gitlab.png){height=500px}

# 改善 (Kaizen)
![](./includes/pdca.png){height=500px}

# 改善 (Kaizen)

* **Plan** : Identifier une action d’amélioration et un élément observable qui pourra permettre de déterminer si l’action a porté ses fruits ou pas.
* **Do** : Faire l’action sur un temps donné, une itération par exemple.
* **Check** : Prendre le temps de mesurer si l’action a apporté le changement souhaité.
* **Act** : Décider, l’action est peut-être à répéter, une autre action peut en découler, etc.

# PDCA

![](./includes/suivi-pdca.png){height=500px}

# Mes attentes

* La démonstration de **toutes** les issues déjà présente
* Le radiateur d’information avec
    * Le nom du projet
    * Les membres de l’équipe
    * Le backlog
    * Le burndown chart
    * Les 3 colonnes de suivi (Todo, In progress, Done)
    * L’expérience d'amélioration en cours

# Mes attentes

    ├── doc
    │   ├── sprint-0
    │   │   ├── radiateur.jpg
    │   │   └── README.md
    ...
    └── src
        ├── main
        │   └── java : les sources de votre projet java
        └── test
            └── java : les sources de vos tests

Avec les issues à jours dans le board

# Sprint O

* Trouver le jeu que vous allez fabriquer
* Partager la vision du jeu (Titre, Slogan)
* Découper et estimer le backlog
* Initier la pile technique et montrer que vous êtes capable de livrer de la valeur.
* Prendre connaissance des 19 issues déjà présentes dans gitlab

📣 La démo de ce sprint c'est au moins le radiateur et la présentation de votre jeu.

# Sprint = 2h

* 1h30 de production
* 30 minutes pour s’améliorer, souffler et fêter la victoire
    * Démo
    * Rétrospective
    * PDCA

# Démarrer

* Rejoindre le projet de votre groupe ici : [https://gitlab.univ-lille.fr/2021-projet-agile-de-rentree](https://gitlab.univ-lille.fr/2021-projet-agile-de-rentree)
* Prendre connaissance des issues déjà présentes dans gitlab

# README.md {.small}

    # Sprint X

    ## Démo + Planification du sprint suivant

    ### Ce que nous avons fait durant ce sprint
    Donnez ici la liste des histoires utilisateurs que vous avez livrées durant ce sprint.
    Vous pouvez utiliser cette liste pour préparer votre démo.

    ### Ce que nous allons faire durant le prochain sprint
    Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

    ## Rétrospective

    ### Sur quoi avons nous butté ?
    Lister ici tout ce qui était un peut moins bien que parfait.
    * Avez-vous terminé tout ce que vous vous étiez engagé à faire ?
    * Étiez -vous prêts au moment de la démo ?
    * ...

    ### PDCA
    * De tous ces éléments quel est celui que vous voulez améliorer ?
    * Comment pouvez-vous mesurer qu'il s'améliore ?
    * Quelles sont toutes les options possible pour l'améliorer ?
    * Qu'allez-vous tester pour l'améliorer ?

    # Mémo
    N’oubliez pas d’ajouter une photo du radiateur d’information au moment de la rétrospective.


# Groupes {class=small}

<div class="small">
<table>
  <tr>
  </tr>
  <tr>
    <td>
      <b>Groupe 0</b><br/>
      <ul>
        <li>BONAL Al.</li>
        <li>DEBAY Lo.</li>
        <li>EL HARROUDI Mo.</li>
        <li>MBAYE Ch.</li>
        <li>TRUFFIN Th.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 1</b><br/>
      <ul>
        <li>ALMEIDA Ne.</li>
        <li>BEN HATAT Le.</li>
        <li>DEMAND Cy.</li>
        <li>MESTARI Ch.</li>
        <li>POTTIER Ch.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 2</b><br/>
      <ul>
        <li>BOULANGER Su.</li>
        <li>CARRUE Tr.</li>
        <li>COLLIN El.</li>
        <li>DEVIGNE Ma.</li>
        <li>SATTI Ya.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 3</b><br/>
      <ul>
        <li>BAUDUIN Ya.</li>
        <li>BEAUDOIN Tr.</li>
        <li>JARASZKIEWICZ Ba.</li>
        <li>LEMARIE Ba.</li>
        <li>PREVOST Si.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 4</b><br/>
      <ul>
        <li>BERNARD Qu.</li>
        <li>BILLIAU Ma.</li>
        <li>HARDELIN Ba.</li>
        <li>LINARDI Pa.</li>
        <li>SENICOURT Ma.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 5</b><br/>
      <ul>
        <li>BLOKKEEL Co.</li>
        <li>CHARLES Bi.</li>
        <li>DEROO Ar.</li>
        <li>MALDEREZ Na.</li>
        <li>VANOORENBERGHE Am.</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>
      <b>Groupe 6</b><br/>
      <ul>
        <li>LALLOYER Ju.</li>
        <li>MOUGEL Vi.</li>
        <li>PIAZZA Re.</li>
        <li>SALOT Em.</li>
        <li>VARNIERE Ko.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 7</b><br/>
      <ul>
        <li>ALIOUAT Ho.</li>
        <li>BRUNIAUX Tr.</li>
        <li>HURET Th.</li>
        <li>POLTORAK Th.</li>
        <li>PUJOL Ab.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 8</b><br/>
      <ul>
        <li>BOCQUET Si.</li>
        <li>DUBART Xa.</li>
        <li>ISMAIL Ay.</li>
        <li>LECOMTE Re.</li>
        <li>RODRIGUES Th.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 9</b><br/>
      <ul>
        <li>CALLENS Ar.</li>
        <li>DE WITTE Ju.</li>
        <li>LAGNEAU Si.</li>
        <li>LEFEBVRE Ro.</li>
        <li>SABINE Am.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 10</b><br/>
      <ul>
        <li>CATTEAU Ju.</li>
        <li>CORNET Ro.</li>
        <li>KOUAKOUA Ad.</li>
        <li>ROUX Hu.</li>
        <li>SCANDELLA An.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 11</b><br/>
      <ul>
        <li>BRICHE Th.</li>
        <li>DELCROIX No.</li>
        <li>LABRE Al.</li>
        <li>VALCKE Ma.</li>
        <li>VENNIN Co.</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>
      <b>Groupe 12</b><br/>
      <ul>
        <li>BERNARD Ma.</li>
        <li>BONAVENTURE Me.</li>
        <li>DUTILLOY Ba.</li>
        <li>MORANT Ar.</li>
        <li>VANROBAEYS Hu.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 13</b><br/>
      <ul>
        <li>BARAN Re.</li>
        <li>DELATTRE Th.</li>
        <li>DESSOLY Ta.</li>
        <li>NEMEGHAIRE Co.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 14</b><br/>
      <ul>
        <li>ADRIAENSSENS El.</li>
        <li>HAZEBROUCQ Vi.</li>
        <li>PAPIN Cl.</li>
        <li>PAREE Hu.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 15</b><br/>
      <ul>
        <li>BASSETT Al.</li>
        <li>DELESTREE Lu.</li>
        <li>DON Sa.</li>
        <li>FORAUX Lo.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 16</b><br/>
      <ul>
        <li>BOUKEBOUT Se.</li>
        <li>FRAPPIER Pa.</li>
        <li>KHAMPAN Ra.</li>
        <li>STORTONI El.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 17</b><br/>
      <ul>
        <li>BERNARD Ma.</li>
        <li>DONNEZ Al.</li>
        <li>MILLEVERT Ma.</li>
        <li>SATTI En.</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>
      <b>Groupe 18</b><br/>
      <ul>
        <li>ALBERT Qu.</li>
        <li>BOUSSERT Am.</li>
        <li>LOUDJEDI Sa.</li>
        <li>MACIEIRA Ma.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 19</b><br/>
      <ul>
        <li>LASSELIN Cl.</li>
        <li>ROUSERE Al.</li>
        <li>SINQUIN Ma.</li>
        <li>VANHEE Pa.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 20</b><br/>
      <ul>
        <li>BAESEN An.</li>
        <li>BIMONT Ma.</li>
        <li>DEMEESTERE Vi.</li>
        <li>SENE Hu.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 21</b><br/>
      <ul>
        <li>ERNOULD Th.</li>
        <li>MANSUE Cl.</li>
        <li>RACHOUANI Ab.</li>
        <li>YANG An.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 22</b><br/>
      <ul>
        <li>PINCEEL Ky.</li>
        <li>STEFFE Ke.</li>
        <li>TACCOEN Th.</li>
        <li>VARLET Sa.</li>
      </ul>
    </td>
    <td>
      <b>Groupe 23</b><br/>
      <ul>
        <li>CAMPAN Do.</li>
        <li>DEGROOTE Al.</li>
        <li>MAGRIT Ga.</li>
        <li>ROUX Cy.</li>
</table></div>


# Salles de TD

| Salle | Groupes |
|-------|---------|
| 0A17  | 0 - 5 - 10 - 15 - 20 |
| 0A18  | 1 - 6 - 11 - 16 - 21 |
| 0A20  | 2 - 7 - 12 - 17 - 22 |
| 0A22  | 3 - 8 - 13 - 18 - 23 |
| 0A24  | 4 - 9 - 14 - 19      |

# Salles de TP

| Salle | Groupes |
|-------|---------|
| 4A02  | 0 - 6  - 12 - 18 |
| 4A04  | 1 - 7  - 13 - 19 |
| 4A12  | 2 - 8  - 14 - 20 |
| 4A14  | 3 - 9  - 15 - 21 |
| 4A18  | 4 - 10 - 16 - 22 |
| 4A20  | 5 - 11 - 17 - 23 |
