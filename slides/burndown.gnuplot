set term svg size 600, 400
set xlabel "Sprint"
set ylabel "Points"
set xrange [0:15]
set yrange [0:60]
set output "public/includes/burndown1.svg"
f1(x) = m * x + q
fit f1(x) 'sprint1.dat' via m, q
plot f1(x) t 'Projection', "sprint1.dat" with linespoints pointtype 7 notitle

set output "public/includes/burndown2.svg"
f2(x) = m2 * x + q2
fit f2(x) 'sprint2.dat' via m2, q2
plot f1(x) t 'Projection 1', f2(x) t 'Projection 2', "sprint2.dat" with linespoints pointtype 7 notitle

set output "public/includes/burndown3.svg"
set xrange [0:25]
f3(x) = m3 * x + q3
fit f3(x) 'sprint3.dat' via m3, q3
plot f1(x) t 'Projection 1', f3(x) t 'Projection 3', "sprint3.dat" with linespoints pointtype 7 notitle

set output "public/includes/burndown4.svg"
set xrange [0:25]
f4(x) = m4 * x + q4
fit f4(x) 'sprint4.dat' via m4, q4
plot f1(x) t 'Projection 1', f3(x) t 'Projection 3', f4(x) t 'Projection', "sprint4.dat" with linespoints pointtype 7 notitle
