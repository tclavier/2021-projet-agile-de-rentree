#!/usr/bin/python3
"""
high level support for doing this and that.
"""

import getopt
import json
import sys
import urllib
import requests
import yaml
from collections import namedtuple

def main(argv):
    """
    main
    """
    token = ''
    project = ''
    host = ''
    try:
        opts, _ = getopt.getopt(argv, "h:t:p:")
    except getopt.GetoptError:
        print('create.py -t <token> -p <projet> -h <host>')
        print('create.py -t $GITLAB_TOKEN -h gitlab.com -p groupe-25')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            host = arg
        elif opt in "-t":
            token = arg
        elif opt in "-p":
            project = arg

    with open(r'all.yml') as file:
        issues = yaml.safe_load(file)

    project_url = urllib.parse.quote("2020-S3-projet/{}".format(project), safe='')
    issues_url = "https://{}/api/v4/projects/{}/issues".format(host, project_url)

    new_project = {"name":project, "namespace_id": 1311}
    print(json.dumps(new_project))
    r = requests.post("https://{}/api/v4/projects/".format(host), data=json.dumps(new_project), headers={"PRIVATE-TOKEN":token, "Content-Type": "application/json"})
    print(r.status_code, r.reason)

    for issue in issues:
        r = requests.post(issues_url, data=json.dumps(issue), headers={"PRIVATE-TOKEN":token, "Content-Type": "application/json"})
        print(r.status_code, r.reason)

if __name__ == "__main__":
    main(sys.argv[1:])
