
#S2-Annee-1-FI.csv  S3-FC.csv  S3-FI.csv

# Build (etudid,  S3 list
cat S3-FI.csv | grep -v AAPPEL | grep -v AAPELL | grep EID | cut -f 1,4 -d ';' | sort > S3.lst

# Build S2 rank file (etudid, rk)
cat S2-Annee-1-FI.csv | grep -v AAPPEL | grep -v AAPELL | grep EID | cut -f 1,3 -d ';' | sort  -t ';' -k 1 > S2.lst

# Take rank on S2 to order in S3
join -t ';' -j 1 -a 1 S3.lst S2.lst | sort -t ';' -k 3 -n > etudiants.lst

# Take FC etudid list
cat S3-FC.csv | grep -v AAPPEL | grep -v AAPELL | grep EID | cut -f 1,4 -d ';' >> etudiants.lst

> groups.lst
IFS_OLD=$IFS
IFS='|'
i=0
for etudiant in $(cat etudiants.lst | cut -f 2 -d ';' | tr '\n' '|')
do
  grp=$(($i%24))
  echo "$etudiant;$grp" >> groups.lst
  i=$(($i+1))
done
IFS=$IFS_OLD


#cat groups.lst | sort -k 2 -t ';' -n | tr ';' '\t'

echo '<div class="small">'
echo '<table>'
echo '  <tr>'

IFS_OLD=$IFS
IFS='|'
LAST_GROUP=-1
for line in $(cat groups.lst | sort -k 2 -t ';' -n| tr '\n' '|')
do
  STUDENT=$(echo $line | cut -f 1 -d ";")
  GROUP=$(echo $line | cut -f 2 -d ";")

  if [ $LAST_GROUP != $GROUP ]
  then
    if [ $GROUP -gt 0 ]
    then
      echo "      </ul>"
      echo "    </td>"
    fi
    GROUP_LINE=$(($GROUP % 6))
    if [ $GROUP_LINE -eq 0 ]
    then
      echo '  </tr>'
      echo '  <tr>'
    fi
    echo "    <td>"
    echo "      <b>Groupe $GROUP</b><br/>"
    echo "      <ul>"
  fi
  echo   "        <li>$STUDENT</li>"
  LAST_GROUP=$GROUP

done
IFS=$IFS_OLD
echo "</table></div>"
