# Git 

## A quoi ça sert ?

Git est un logiciel qui permet la gestion de versions de code. Il permet d’enregistrer régulièrement son travail, de revenir sur des versions précédentes mais aussi de travailler avec d’autres développeurs grâce à des capacités de fusion de code.

Gitlab est une interface web pour visualiser l’état d’un repository git. D’autres fonctionnalités utiles aux développeurs ont été ajoutées autour de cette fonctionnalité de base comme une todo list ou un wiki.

## Comment l’utiliser ?

Dans un premier temps il faut cloner le dépôt distant sur votre machine.

    git clone https://gitlab.univ-lille.fr/2020-S3-projet/groupe-XXXXX.git
    cd groupe-XXXXX

Ensuite vous pouvez developper normalement dans ce répertoire et à chaque fois que vous souhaitez enregistrer votre code faire :

1. Je séléctionne les modications à inclure avec `git add NOM_DU_FICHIER` pour chaque fichier
2. Je crée la nouvelle étape avec `git commit -m "quoi de nouveau dans cette version"`
3. J’envoie sur le serveur distant avec `git push`, cette commande peut indiquer une erreur si il y a des modifications qui ont été ajoutées entre temps. Il faudra alors récupérer ces modifications avec `git pull` qui va synchroniser le repertoire distant avec celui de votre machine.

Je peux voir les fichiers sélectionnés et ceux qui ont été modifiés depuis mon dernier commit avec `git status`.

## Conflits

Parfois, un même fichier est modifié au même endroit par plusieurs personnes en même temps, git ne peut savoir quelle version choisir. On appelle cela un merge conflict. Ce n’est pas grave, il va falloir éditer les fichiers problématiques pour y enlever les caractères ajoutes par git (<<<<< HEAD etc) ainsi que la version inutile.

Par exemple si j’ai ceci dans un fichier après un conflit : 

    <<<<<<< HEAD
    #Ceci est une version obsolète
    int x = 1 ;
    =======
    #Ceci est une autre version
    int x = 3 ;
    >>>>>>> branch

Je le modifie pour ne garder que les lignes :

    #Ceci est une autre version
    int x = 3 ;

Ensuite j’indique à git que j’ai résolu le conflit avec `git commit -m "conflit résolu sur le fichier xxxx.xx"`

## Pour aller plus loin

Mettre en cache ses identifiants pendant 4 heures : `git config credential.helper 'cache --timeout 14400'`



Git est un outil complexe, pour l'instant une utilisation simplifiée est préférable mais par la suite vous aller en découvrir plus.

D'autres commandes et explications sont indiquées ici https://raw.githubusercontent.com/tclavier/memo-git/master/memo.pdf afin d'apprendre à utiliser ces concepts de git :

* Branches
* Naviguer dans les commits
* Revenir en arrière
* .gitignore pour ignorer certains fichiers 
